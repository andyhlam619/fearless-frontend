// Get the cookie out of the cookie store
const payloadCookie = await cookieStore.get("jwt_access_payload");
if (payloadCookie) {
    // The cookie value is a JSON-formatted string, so parse it
    const encodedPayload = JSON.parse(payloadCookie.value);
    console.log(encodedPayload)

    // Convert the encoded payload from base64 to normal string
    const decodedPayload = atob(encodedPayload)
    console.log(decodedPayload)

    // The payload is a JSON-formatted string, so parse it
    const payload = JSON.parse(decodedPayload)
    console.log(payload);

    // Print the payload

    // Check if "events.add_conference" is in the permissions.
    // If it is, remove 'd-none' from the link

    const perms = payload.user.perms
    if (perms.includes("events.add_conference")) {
        const locationNav = document.getElementById('newconference')
        locationNav.classList.remove("d-none")
    }
    if (perms.includes("events.add_location")) {
        const locationNav = document.getElementById('newlocation')
        locationNav.classList.remove("d-none")
    }
    if (perms.includes("presentations.add_presentation")) {
        const locationNav = document.getElementById('newpresentation')
        locationNav.classList.remove("d-none")
    }

}
